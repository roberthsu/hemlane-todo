import routes from '../../web/Utilities/Config/routes.js'


before(() => {
  cy.fixture('users/new_manager').as('manager')
})

describe('Go to Registration', () => {
  it ('Goes to the correct url', () => {
    cy.visit('/registration/manager')
    cy.location("pathname")
    .should('eq', routes.managerRegistration)
  })
})

describe('Navigation bar buttons', () => {
  it ('Menu dropdown options', () => {
    cy.get('div[data-test=nav-bar-menu]')
      .click()

    cy.get('div[class=customer-support-clickable-area]')
      .click()

    cy.get('div[data-test=customer-support-modal]')
      .should('be.visible')

    cy.get('button[data-test=customer-support-modal-done]')
      .click()

    cy.get('div[data-test=customer-support-modal]')
      .should('not.be.visible')
  })

  it ('Finret button reroutes to homepage', () => {
    cy.get('a[data-test=nav-bar-logo]')
      .click()

    cy.location('pathname')
      .should('eq', '/')

    cy.visit('/registration/manager')
  })
})

describe('URL Input', () => {
  it ('Display error on invalid url', function() {
    cy.get('input[name=applicationUrl]')
      .type(this.manager.badUrl)

    cy.get('button[data-test=url-submit]')
      .click()

    cy.get('div[data-test=url-error]')
      .should('be.visible')
      .and('contain.text', 'Invalid URL')
  })

  it ('Display error on existing url', function() {
    cy.get('input[name=applicationUrl]')
      .clear()
      .type(this.manager.existingUrl)

    cy.get('button[data-test=url-submit]')
      .click()

    cy.get('div[data-test=url-error]')
      .should('be.visible')
      .and('contain.text', 'Error registering URL')
  })

  it ('Goes to next view on valid url', function() {
    cy.get('input[name=applicationUrl]')
      .clear()
      .type(this.manager.goodUrl)

    cy.get('button[data-test=url-submit]')
      .click()

    cy.get('h3')
      .should('contain.text', 'Add Your Personal Information')
  })
})

describe('Personal Information Form', () => {
  it ('Display error on invalid email', function() {
    cy.fillPersonalInfoForm(this.manager)

    cy.get('input[name=email]')
      .clear()
      .type(this.manager.badEmail)

    cy.get('button[data-test=personal-info-submit]')
      .click({force:true})

    cy.get('div[data-test=personal-info-error]')
      .should('be.visible')
      .and('contain.text', 'Invalid Email')
  })

  it ('Display error on existing email', function() {
    cy.get('input[name=email]')
      .clear()
      .type(this.manager.existingEmail)

    cy.get('button[data-test=personal-info-submit]')
      .click({force:true})

    cy.get('div[data-test=personal-info-error]')
      .should('be.visible')
      .and('contain.text', 'Your email has already been used')

    cy.get('input[name=email]')
      .clear()
      .type(this.manager.goodEmail)
  })

  it ('Display error on bad first name', function() {
    cy.get('input[name=firstName]')
      .clear()
      .type(this.manager.badFirstName)

    cy.get('button[data-test=personal-info-submit]')
      .click({force:true})

    cy.get('div[data-test=personal-info-error]')
      .should('be.visible')
      .and('contain.text', 'Invalid Name')

    cy.get('input[name=firstName]')
      .clear()
      .type(this.manager.goodFirstName)
  })

  it ('Display error on bad last name', function() {
    cy.get('input[name=lastName]')
      .clear()
      .type(this.manager.badLastName)

    cy.get('button[data-test=personal-info-submit]')
      .click({force:true})

    cy.get('div[data-test=personal-info-error]')
      .should('be.visible')
      .and('contain.text', 'Invalid Name')

    cy.get('input[name=lastName]')
      .clear()
      .type(this.manager.goodLastName)
  })

  it('Display error on bad password', function() {
    cy.get('input[name=password]')
      .clear()
      .type(this.manager.badPassword)

    cy.get('input[name=confirmPassword]')
      .clear()
      .type(this.manager.badPassword)

    cy.get('button[data-test=personal-info-submit]')
      .click({force:true})

    cy.get('div[data-test=personal-info-error]')
      .should('be.visible')
      .and('contain.text', 'Invalid Password')
  })

  it ('Display error on mismatched password', function() {
    cy.get('input[name=password]')
      .clear()
      .type(this.manager.goodPassword)

    cy.get('input[name=confirmPassword]')
      .clear()
      .type(this.manager.badPassword)

    cy.get('button[data-test=personal-info-submit]')
      .click({force:true})

    cy.get('div[data-test=personal-info-error]')
      .should('be.visible')
      .and('contain.text', 'Passwords do not match')

    cy.get('input[name=confirmPassword]')
      .clear()
      .type(this.manager.goodPassword)
  })

  it ('Goes to next view on valid inputs', function() {
    cy.get('button[data-test=personal-info-submit]')
      .click({force:true})

    cy.get('h3')
      .should('contain.text', 'Add Your Business Information')
  })
})

describe('Business Information Form', () => {
  it ('Display error on invalid phone number', function() {
    cy.fillBusinessInfoForm(this.manager)

    cy.get('div[class="react-tel-input"] input')
      .clear()
      .type(this.manager.badPhoneNumber)

    cy.get('button[data-test=business-info-submit]')
      .click({force:true})

    cy.get('div[data-test=business-info-error]')
      .should('be.visible')
      .and('contain.text', 'Invalid Phone Number')

    cy.get('div[class="react-tel-input"] input')
      .clear()
      .type(this.manager.goodPhoneNumber)
  })

  it ('Display error on invalid street address', function() {
    cy.get('input[name=streetAddress]')
      .clear()
      .type(this.manager.badStreetAddress)

    cy.get('button[data-test=business-info-submit]')
      .click({force:true})

    cy.get('div[data-test=business-info-error]')
      .should('be.visible')
      .and('contain.text', 'Invalid Street Address')

    cy.get('input[name=streetAddress]')
      .clear()
      .type(this.manager.goodStreetAddress)
  })

  it ('Display error on invalid city', function() {
    cy.get('input[name=city]')
      .clear()
      .type(this.manager.badCity)

    cy.get('button[data-test=business-info-submit]')
      .click({force:true})

    cy.get('div[data-test=business-info-error]')
      .should('be.visible')
      .and('contain.text', 'Invalid City')

    cy.get('input[name=city]')
      .clear()
      .type(this.manager.goodCity)

  })
})

describe('Back and Continue Buttons', () => {
  it ('Back buttons switch to previous view and preserve inputs', function() {
    cy.get('button[data-test=business-info-back]')
      .click({force:true})

    cy.checkPersonalInfoForm(this.manager)

    cy.get('button[data-test=personal-info-back]')
      .click({force:true})

    cy.get('h3')
      .should('contain.text', 'Create Your URL')

    cy.get('input[name=applicationUrl]')
      .should('have.value', this.manager.goodUrl)
  })

  it ('Continue buttons switch to next view and preserve inputs', function() {
    cy.get('button[data-test=url-submit]')
      .click({force:true})

    cy.checkPersonalInfoForm(this.manager)

    cy.get('button[data-test=personal-info-submit]')
      .click({force:true})

    cy.get('h3')
      .should('contain.text', 'Add Your Business Information')

    cy.get('input[name=businessName]')
      .should('have.value', this.manager.businessName)

      //TODO: deal with react-tel-input autoformatting against plain string
    // cy.get('div[class="react-tel-input"] input')
    //   .should('have.value', this.manager.goodPhoneNumber)

    cy.get('input[name=streetAddress]')
      .should('have.value', this.manager.goodStreetAddress)

    cy.get('input[name=aptNumber]')
      .should('have.value', this.manager.aptNumber)

    cy.get('input[name=city]')
      .should('have.value', this.manager.goodCity)

      //TODO: deal with verifying dropdown selection
    // cy.get('div[name=state] input')
    //   .should('have.value', this.manager.state)

    cy.get('input[name=zipCode]')
      .should('have.value', this.manager.zipCode)
  })
})

describe('Redirect on submission of valid inputs', () => {
    it ('Submit and redirect to manager dashboard', function() {
      cy.get('button[data-test=business-info-submit]')
        .click({force:true})

      cy.location("pathname")
      .should('eq', routes.managementDashboard)
    })
})
