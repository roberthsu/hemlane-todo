before(function () {
  cy.fixture('users/manager').as('manager')
  cy.fixture('users/false_user').as('false_user')
  cy.visit('/')
  cy.location('pathname').should('eq', '/')
  cy.get('button[data-test="sign-in-desktop"]')
    .click()
})

describe('Login from Homepage', () => {
  it('has a link to reset password', () => {
    cy.contains('Reset password here')
      .should('have.attr', 'href', '/password/reset')
  })

  it('Requires email', () => {
    cy.get('button[data-test="login"]').click()
    cy.get('div[data-test="error-message"]').should('be.visible')
  })

  it('Requires password', function() {
    cy.get('div[data-test="username"] input')
      .type(this.false_user.email)
    cy.contains('Login').click()
    cy.get('div[data-test="error-message"]').should('be.visible')
  })

  it('Tries to login unsuccessfully', function() {
    cy.get('div[data-test="username"] input')
      .clear()
      .type(this.false_user.email)
      .should('have.value', this.false_user.email)

    cy.get('div[data-test="password"] input')
      .type(this.false_user.password)
      .should('have.value', this.false_user.password)

    cy.get('button[data-test="login"]')
      .click()

    cy.location('pathname').should('eq', '/')
  })

  it('Tries to login successfully', function() {
    cy.get('div[data-test="username"] input')
      .clear()
      .type(this.manager.email)
      .should('have.value', this.manager.email)

    cy.get('div[data-test="password"] input')
      .clear()
      .type(this.manager.password)
      .should('have.value', this.manager.password)

    cy.get('button[data-test="login"]')
      .click()

    cy.location('pathname').should('eq', '/management/dashboard')
  })
})