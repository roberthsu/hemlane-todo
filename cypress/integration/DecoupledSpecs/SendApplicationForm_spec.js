before('Applicant Dashboard', () => {
  cy.fixture("users/applicant").as("applicant")
  cy.fixture("users/manager").as("manager")
  cy.fixture("users/false_user").as("false_user")
})

describe('Send Application Form', () => {
  before (function () {
    cy.login(this.applicant.email, this.applicant.password)
    cy.location("pathname").should('eq', '/applicant/dashboard')
    cy.get('div[data-test="send-application-tab"]')
      .click()
  })
  beforeEach(function() {
    Cypress.Cookies.preserveOnce('PHPSESSID')
    cy.server()
  })

  it ('Email must not be in use by an applicant', function() {
    cy.get('div[data-test="manager-email"] input')
      .clear()
      .type(this.applicant.email)

    cy.get('div[data-test="property-nickname"] input')
      .clear()
      .type(this.manager.propertyNickname)

    cy.get('div[data-test="expected-rent"] input')
      .clear()
      .type(this.manager.expectedRent)

    cy.get('div[data-test="street-address"] input')
      .clear()
      .type(this.manager.streetAddress)

    cy.get('div[data-test="city"] input')
      .clear()
      .type(this.manager.city)

    cy.get('div[data-test="state"]')
      .click()
    cy.get('div[data-test="state"] input')
      .type(this.manager.state)

    cy.get('div[data-test="zipcode"] input')
      .clear()
      .type(this.manager.zipcode)

    cy.route('POST', '/applicant/application/share').as('postShareApplication')
    cy.get('button[data-test="send"]')
      .click()
    cy.wait('@postShareApplication', {timeout: 10000}).its('status').should('eq', 409)
    cy.get('div[data-test="error-message"]').should('be.visible')
  }) 

  it('Sends an Application', function() {
    cy.get('div[data-test="manager-email"] input')
      .clear()
      .type(this.manager.email)

    cy.get('div[data-test="first-name"] input')
      .clear()
      .type(this.manager.firstName)

    cy.get('div[data-test="last-name"] input')
      .clear()
      .type(this.manager.lastName)

    cy.get('div[data-test="property-nickname"] input')
      .clear()
      .type(this.manager.propertyNickname)

    cy.get('div[data-test="expected-rent"] input')
      .clear()
      .type(this.manager.expectedRent)

    cy.get('div[data-test="street-address"] input')
      .clear()
      .type(this.manager.streetAddress)

    cy.get('div[data-test="city"] input')
      .clear()
      .type(this.manager.city)

    cy.get('div[data-test="state"]')
      .click()
    cy.get('div[data-test="state"] input')
      .type(this.manager.state)

    cy.get('div[data-test="zipcode"] input')
      .clear()
      .type(this.manager.zipcode)

      cy.route('POST', '/applicant/application/share').as('postShareApplication')
      cy.get('button[data-test="send"]')
        .click()
      cy.wait('@postShareApplication', {timeout: 10000}).its('status').should('eq', 200)
  })
})