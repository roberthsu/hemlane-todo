/// <reference types="Cypress" />

describe('Go to Registration', () => {
  it ('should go to the correct url', () => {
    cy.visit('/registration/applicant')
    cy.location("pathname").should('eq', '/registration/applicant')
  })

  it('has a link to login', () => {
    cy.contains('Already have an account? Sign in.')
      .should('have.attr', 'href', '/')
  })
})

describe('Registration Form - Applicant', () => {
  beforeEach(() => {
    cy.get('div[data-test="first-name"] input').clear().type('John')
    cy.get('div[data-test="last-name"] input').clear().type('Wayne')
    cy.get('div[data-test="email"] input').clear().type('feaohfh@gmail.com')
    cy.get('div[data-test="phone-number"] input').clear().type('1415112525')
    cy.get('div[data-test="password"] input').clear().type('password')
    cy.get('div[data-test="confirm-password"] input').clear().type('password')
  })

  it('Does not allow names with numbers or spaces', () => {
    cy.get('div[data-test="first-name"] input').clear().type('John II')
    cy.get('button[data-test="submit"]').click()
    cy.get('div[data-test="error-message"]').should('be.visible')
    cy.get('div[data-test="first-name"] input').clear().type('John2')
    cy.get('button[data-test="submit"]').click()
    cy.get('div[data-test="error-message"]').should('be.visible')
    cy.get('div[data-test="first-name"] input').clear().type('John')
    cy.get('div[data-test="last-name"] input').clear().type('Wayne II')
    cy.get('button[data-test="submit"]').click()
    cy.get('div[data-test="error-message"]').should('be.visible')
    cy.get('div[data-test="last-name"] input').clear().type('Wayne2')
    cy.get('button[data-test="submit"]').click()
    cy.get('div[data-test="error-message"]').should('be.visible')
  })

  it('has Phone Number validation', function() {
    cy.get('div[data-test="phone-number"] input').clear().type('14153')
    cy.get('button[data-test="submit"]').click()
    cy.get('div[data-test="error-message"]').should('be.visible')
  })

  it('has Password length validation', function() {
    cy.get('div[data-test="password"] input').clear().type('passwor')
    cy.get('div[data-test="confirm-password"] input').clear().type('passwor')
    cy.get('button[data-test="submit"]').click()
    cy.get('div[data-test="error-message"]').should('be.visible')
  })

  it('has Password match validation', function() {
    cy.get('div[data-test="password"] input').clear().type('password')
    cy.get('div[data-test="confirm-password"] input').clear().type('passworx')
    cy.get('button[data-test="submit"]').click()
    cy.get('div[data-test="error-message"]').should('be.visible')
  })
})