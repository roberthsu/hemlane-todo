/// <reference types="Cypress" />

beforeEach(() => {
  cy.fixture("users/manager").as("manager")
  Cypress.Cookies.preserveOnce('PHPSESSID')
  cy.server()
})

describe('Go to Homepage', () => {
  it ('should go to the correct url', () => {
    cy.visit('/')
  })
})

describe('Log in as manager', () => {
  it('tries to login unsuccessfully', function() {
    cy.get('div[class="item"] button')
      .click()
    
    cy.get('input[name="username"]')
      .type(this.false_user.email)
      .should("have.value", this.false_user.email)

    cy.get('input[name="password"]')
      .type(this.false_user.password)
      .should("have.value", this.false_user.password)

    cy.get('form button')
      .click()

    cy.location("pathname").should('eq', '/')
  })

  it('tries to login successfully', function() {
    cy.get('input[name="username"]')
      .clear()
      .type(this.manager.email)
      .should("have.value", this.manager.email)

    cy.get('input[name="password"]')
      .clear()
      .type(this.manager.password)
      .should("have.value", this.manager.password)

    cy.get('form button')
      .click()

    cy.location("pathname").should('eq', '/management/dashboard')
  })
})