/// <reference types="Cypress" />
before('Applicant Dashboard', () => {
  cy.fixture("users/applicant").as("applicant")
  cy.fixture("users/manager").as("manager")
  cy.fixture("data/landlord_reference").as("landlord")
  cy.fixture("data/landlord_reference_edit").as("newLandlord")
  cy.fixture("data/employment_detail").as("employmentDetail")
  cy.fixture("data/employment_detail_edit").as("newEmploymentDetail")
  cy.fixture("data/renter_questions").as("renterQuestions")
})

describe('Applicant Dashboard', () => {
  before (function () {
    cy.login(this.applicant.email, this.applicant.password)
    cy.location("pathname").should('eq', '/applicant/dashboard')
  })
  beforeEach(function() {
    Cypress.Cookies.preserveOnce('PHPSESSID')
    cy.server()
  })

  it('Clears the welcome modal (OK to fail if document exists)', () => {
    cy.get('button[data-test="got-it"]')
      .click()
  })

  it('Adds a document', () => {
    cy.get('button[data-test="add-document"]')
      .click()

    cy.get('div[data-test="employment-status"]')
      .click()
      .type('{enter}')

    cy.get('div[data-test="child-support-status"]')
      .click()
      .type('{enter}')

    cy.get('div[data-test="alimony-status"]')
      .click()
      .type('{enter}')

    cy.get('button[data-test="continue"]')
      .click()

    cy.get('div[data-test="identification-options"]')
      .click()
      .type('{enter}')

    cy.get('button[data-test="choose-file"]').first()
      .click()
      .pause()
      //select file to upload, then resume. Please upload 2 documents so that deleting one won't make the intro popup

    /* cy.route('POST', "/applicant/report/document").as('postUploadDocument')
    cy.wait('@postUploadDocument', {timeout: 30000}) */
    
    cy.route('GET', "/applicant/report/document").as('getUploadDocument')
    cy.get('button[data-test="done"]')
      .click()
    cy.wait('@getUploadDocument', {timeout: 10000}).its('status').should('eq', 200)
  })

  it('Views a Document', function() {
    cy.get('div[data-test="view-document"]').first()
      .click()
    cy.pause() //confirm visuals, then resume
  })

  it('Deletes a Document', function() {
    cy.route('DELETE', "/applicant/report/document").as('deleteUploadDocument')
    cy.get('div[data-test="delete-document"]').first()
      .click()
    cy.wait('@deleteUploadDocument', {timeout: 10000}).its('status').should('eq', 200)
  })

  it('Adds a Landlord Reference', function() {
    cy.get('button[data-test="add-reference"]')
      .click()

    cy.get('div[data-test="landlord-name"] input')
      .type(this.landlord.landlordName)

    cy.get('div[data-test="property-address"] input')
      .type(this.landlord.propertyAddress)

    cy.get('div[data-test="city"] input')
      .type(this.landlord.city)

    cy.get('div[data-test="state"] input')
      .type(this.landlord.state)

    cy.get('div[data-test="zipcode"] input')
      .type(this.landlord.zipcode)

    cy.get('div[data-test="phone-number"] div[class="react-tel-input"] input')
      .type(this.landlord.phoneNumber)

    cy.route('POST', "/applicant/report/reference").as('postLandlordReference')
    cy.route('GET', "/applicant/report/reference").as('getLandlordReference')
    cy.get('button[data-test="done"]')
      .click()
    cy.wait('@postLandlordReference', {timeout: 10000}).its('status').should('eq', 200)
    cy.wait('@getLandlordReference', {timeout: 10000}).its('status').should('eq', 200)
  })

  it('Edit a Landlord Reference', function() {
    cy.get('div[data-test="edit-reference"]').first()
      .click()
    
    cy.get('div[data-test="landlord-name"] input')
      .clear()
      .type(this.newLandlord.landlordName)

    cy.get('div[data-test="property-address"] input')
      .clear()
      .type(this.newLandlord.propertyAddress)

    cy.get('div[data-test="city"] input')
      .clear()
      .type(this.newLandlord.city)

    cy.get('div[data-test="state"] input')
      .clear()
      .type(this.newLandlord.state)

    cy.get('div[data-test="zipcode"] input')
      .clear()
      .type(this.newLandlord.zipcode)

    cy.get('div[data-test="phone-number"] div[class="react-tel-input"] input')
      .clear()
      .type(this.newLandlord.phoneNumber)

    cy.route('PUT', "/applicant/report/reference").as('putLandlordReference')
    cy.route('GET', "/applicant/report/reference").as('getLandlordReference')
    cy.get('button[data-test="done"]')
      .click()
    cy.wait('@putLandlordReference', {timeout: 10000}).its('status').should('eq', 200)
    cy.wait('@getLandlordReference', {timeout: 10000}).its('status').should('eq', 200)
  })

  it('Deletes a Landlord Reference', function() {
    cy.route('DELETE', "/applicant/report/reference").as('deleteLandlordReference')
    cy.get('div[data-test="delete-reference"]').first()
      .click()
    cy.wait('@deleteLandlordReference', {timeout: 10000}).its('status').should('eq', 200)
  })

  it ('Adds an Employment Detail', function() {
    cy.get('button[data-test="add-employer"]')
      .click()

    cy.get('div[data-test="employer"] input')
      .type(this.employmentDetail.employer)

    cy.get('div[data-test="manager"] input')
      .type(this.employmentDetail.manager)

    cy.get('div[data-test="monthly-income"] input')
      .type(this.employmentDetail.monthlyIncome)

    cy.get('div[data-test="phone-number"] div[class="react-tel-input"] input').first()
      .type(this.employmentDetail.phoneNumber)

    cy.route('POST', "/applicant/report/employment-detail").as('postEmploymentDetail')
    cy.route('GET', "/applicant/report/employment-detail").as('getEmploymentDetail')
    cy.get('button[data-test="done"]')
      .click()
    cy.wait('@postEmploymentDetail', {timeout: 10000}).its('status').should('eq', 200)
    cy.wait('@getEmploymentDetail', {timeout: 10000}).its('status').should('eq', 200)
  })

  it('Edit an Employment Detail', function() {
    cy.get('div[data-test="edit-employment"]').first()
      .click()
    
    cy.get('div[data-test="employer"] input')
      .clear()
      .type(this.newEmploymentDetail.employer)

    cy.get('div[data-test="manager"] input')
      .clear()
      .type(this.newEmploymentDetail.manager)

    cy.get('div[data-test="monthly-income"] input')
      .clear()
      .type(this.newEmploymentDetail.monthlyIncome)

    cy.get('div[data-test="phone-number"] div[class="react-tel-input"] input').first()
      .clear()
      .type(this.newEmploymentDetail.phoneNumber)

    cy.route('PUT', "/applicant/report/employment-detail").as('putEmploymentDetail')
    cy.route('GET', "/applicant/report/employment-detail").as('getEmploymentDetail')
    cy.get('button[data-test="done"]')
      .click()
    cy.wait('@putEmploymentDetail', {timeout: 10000}).its('status').should('eq', 200)
    cy.wait('@getEmploymentDetail', {timeout: 10000}).its('status').should('eq', 200)
  })

  it('Deletes an Employment Detail', function() {
    cy.route('DELETE', "/applicant/report/employment-detail").as('deleteEmploymentDetail')
    cy.get('div[data-test="delete-employment"]').first()
      .click()
    cy.wait('@deleteEmploymentDetail', {timeout: 10000}).its('status').should('eq', 200)
  })

  it('Edit Renter Question Modal', function() {
    cy.get('div[data-test="renter-question-modal"]')
      .click()

    cy.get('div[data-test="move-date"]')
      .click()
      .type('{enter}')

    cy.get('div[data-test="ideal-duration"]')
      .click()
      .type('{enter}')

    cy.get('div[data-test="moving-reason"] input')
      .clear()
      .type(this.renterQuestions.movingReason)

    cy.get('div[data-test="household-members"] input')
      .clear()
      .type(this.renterQuestions.householdMembers)

    cy.get('div[data-test="min-budget"] input')
      .clear()
      .type(this.renterQuestions.minBudget)

    cy.get('div[data-test="max-budget"] input')
      .clear()
      .type(this.renterQuestions.maxBudget)

    cy.get('div[data-test="square-footage"] input')
      .clear()
      .type(this.renterQuestions.squareFootage)

    cy.get('div[data-test="amenities"] input')
      .clear()
      .type(this.renterQuestions.amenities)

    cy.get('div[data-test="recently-viewed-credit"] input')
      .clear()
      .type(this.renterQuestions.recentlyViewedCredit)

    cy.get('div[data-test="rental-reason"] input')
      .clear()
      .type(this.renterQuestions.rentalReason)

    cy.get('div[data-test="free-pizza"]')
      .click()

    cy.get('div[data-test="first-month-discount"]')
      .click()

    cy.get('div[data-test="other"]')
      .click()

    cy.get('div[data-test="incentives"] input')
      .clear()
      .type(this.renterQuestions.incentivesOther)

    cy.get('textarea[data-test="self"]')
      .clear()
      .type(this.renterQuestions.self)

    cy.get('button[data-test="save"]')
      .click()
  })

  it('Sends an Application', function() {
    cy.get('div[data-test="send-application-tab"]')
      .click()

    cy.get('div[data-test="manager-email"] input')
      .clear()
      .type(this.manager.email)

    cy.get('div[data-test="first-name"] input')
      .clear()
      .type(this.manager.firstName)

    cy.get('div[data-test="last-name"] input')
      .clear()
      .type(this.manager.lastName)

    cy.get('div[data-test="property-nickname"] input')
      .clear()
      .type(this.manager.propertyNickname)

    cy.get('div[data-test="expected-rent"] input')
      .clear()
      .type(this.manager.expectedRent)

    cy.get('div[data-test="street-address"] input')
      .clear()
      .type(this.manager.streetAddress)

    cy.get('div[data-test="city"] input')
      .clear()
      .type(this.manager.city)

    cy.get('div[data-test="state"]')
      .click()
    cy.get('div[data-test="state"] input')
      .type(this.manager.state)

    cy.get('div[data-test="zipcode"] input')
      .clear()
      .type(this.manager.zipcode)

      cy.route('POST', '/applicant/application/share').as('postShareApplication')
      cy.get('button[data-test="send"]')
        .click()
      cy.wait('@postShareApplication', {timeout: 10000}).its('status').should('eq', 200)
  })
})