/// <reference types="Cypress" />

//Feel free to customize this spec to test specific parts of the Registration Flow for any accounts that have not finished it.
beforeEach(() => {
  cy.fixture("users/applicant").as("applicant")
  cy.fixture("users/new_applicant").as("newapplicant")
  Cypress.Cookies.preserveOnce('PHPSESSID')
  cy.server()
})

describe('Go to Homepage', () => {
  it ('should go to the correct url', () => {
    cy.visit('/')
  })
})

//Log in to go back to flow
describe('Log in as applicant', () => {

  it('tries to login successfully', function() {
    cy.get('div[class="item"] button')
      .click()
    
    cy.get('input[name="username"]')
      .type(this.applicant.email)
      .should("have.value", this.applicant.email)

    cy.get('input[name="password"]')
      .type(this.applicant.password)
      .should("have.value", this.applicant.password)

    cy.get('form button')
      .click()
    cy.location("pathname").should('eq', '/registration/applicant')
  })
})

describe('Smartmove and IDMA', () => {
  it('Agrees to Terms', () => {
    cy.get('div[data-test="i-agree-to-these-terms"]')
      .click()
    
    cy.get('button[data-test="filled-button"]')
      .click()
  })
  
  it('Fills out Smartmove Application', function() {
    cy.get('div[data-test="streetAddress"] input')
      .type(this.applicant.streetAddress)
    
    cy.get('div[data-test="city"] input')
      .type(this.applicant.city)
  
    cy.get('div[data-test="state"] input')
      .type(this.applicant.state)
  
    cy.get('div[data-test="zipCode"] input')
      .type(this.applicant.zipcode)
  
    cy.get('div[data-test="ssn"] input')
      .type(this.applicant.ssn)
  
    cy.get('div[name="employment"]')
      .click()
      .type('{enter}')
  
    cy.get('div[data-test="income"] input')
      .type(this.applicant.yearlyIncome)
  
    cy.route('POST', "/applicant/dashboard/update/personal-information").as('postPersonalInfo')
    cy.route('POST', "/applicant/smartmove/finret-generate-application").as('postSmartmove')
  
    cy.get('button[data-test="submit"]')
      .click()
    cy.wait(['@postPersonalInfo', '@postSmartmove'], {timeout: 30000})
  })
  
  it('Starts IDMA verification', () => {
    cy.get('button') //Start
      .click()
  
    cy.get('div label').first() //#1
      .click()
    cy.get('button') //next
      .click()
  
    cy.get('div label').first() //#2
      .click()
    cy.get('button') //next
      .click()
  
    cy.route('POST', "/applicant/smartmove/submit-idma-exam").as('submitIdma')
    cy.get('div label').first() //#3
    .click()
    cy.get('button') //submit
    .click()
    cy.wait('@submitIdma')
  
    cy.get('button') //continue
      .click()
    cy.wait(10000)
  })
})

describe('Payment', () => {
  it('Fills out Square Payment Form', function() {
    cy.get('input[placeholder="Name"]')
      .type(this.applicant.sqpaymentName)
    
    cy.get('iframe[id="sq-card-number"]').pause()
    //manual input required
  
    //resume here
    cy.route('POST', '/purchase/application').as('purchaseApplication')
    cy.get('button[data-test="purchase"]')
      .click()
    cy.wait('@purchaseApplication', {timeout: 10000})
  })
  
  it('Authorizes Financial Information Release', function() {
    cy.get('button[data-test="authorize-financial-information-release"]').pause()
    //manual input required
  
    /* cy.get('button[class="Button Button--is-plaid-color"]')
      .click()
  
    cy.get('li[data-institution="citi"]')
      .click()
  
    cy.get('input[placeholder="User ID"]')
      .type(this.newapplicant.bankUser)
  
    cy.get('input[placeholder="Password"]')
      .type(this.newapplicant.bankPassword)
  
    cy.route('POST', '/plaid/exchange-token').as('plaid')
    cy.get('button[type="submit"]') //submit
      .click()
    cy.get('button[data-test="continue"]') //continue
      .click()
    cy.wait('@plaid')
    cy.route('POST', '/applicant/application/share').as('shareApplication')
    cy.get('button[role="button"]')
      .click()
    cy.wait('@shareApplication') */
  })
})

describe('Confirmation Code and GoToDashboard', () => {
  //Happy Test
  it('uses the correct code and goes to dashboard', function() {
    //resume here
    cy.get('button[data-test="review-application"]')
      .click()
  
    cy.location("pathname").should('eq', '/applicant/dashboard')
  })
})