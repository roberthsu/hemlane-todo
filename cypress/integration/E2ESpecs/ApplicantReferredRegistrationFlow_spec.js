/// <reference types="Cypress" />
beforeEach(() => {
  cy.fixture("users/new_applicant").as("applicant")
  Cypress.Cookies.preserveOnce('PHPSESSID')
  cy.server()
})

describe('Go to Referral Registration', () => {
  it ('should go to the correct url', function() {
    cy.visit(this.applicant.referralUrl)
    cy.location("pathname").should('eq', this.applicant.referralUrl)
  })
})

//Do not decouple this from the rest of the registration flow! It's more important that the flow works
describe('Registration Flow - Referred Applicant', () => {
  //The happy test
  it('should successfully create a new account', function() {
    cy.get('div[data-test="first-name"] input')
      .type(this.applicant.firstName)
    
    cy.get('div[data-test="last-name"] input')
      .type(this.applicant.lastName)

    cy.get('div[data-test="email"] input')
      .type(this.applicant.email)

    cy.get('div[data-test="phone-number"] div[class="react-tel-input"] input')
      .type(this.applicant.phoneNumber)

    cy.get('div[data-test="password"] input')
      .type(this.applicant.password)

    cy.get('div[data-test="confirm-password"] input')
      .type(this.applicant.password)

    cy.route('POST', '/security/login').as('postAuth')
    cy.get('button[data-test="submit"]')
      .click()
    cy.wait('@postAuth', {timeout: 10000}).its('status').should('eq', 200)
  })
})

describe('Smartmove and IDMA', () => {
  it('Agrees to Terms', () => {
    cy.get('div[data-test="i-agree-to-these-terms"]')
      .click()
    
    cy.get('button[data-test="filled-button"]')
      .click()
  })
  
  it('Fills out Smartmove Application', function() {
    cy.get('div[data-test="street-address"] input')
      .type(this.applicant.streetAddress)
    
    cy.get('div[data-test="city"] input')
      .type(this.applicant.city)
  
    cy.get('div[data-test="state"] input')
      .type(this.applicant.state)
  
    cy.get('div[data-test="zipcode"] input')
      .type(this.applicant.zipcode)
  
    cy.get('div[data-test="ssn"] input')
      .type(this.applicant.ssn)
  
    cy.get('div[name="employment"]')
      .click()
      .type('{enter}')
  
    cy.get('div[data-test="income"] input')
      .type(this.applicant.yearlyIncome)
  
    cy.route('POST', "/applicant/dashboard/update/personal-information").as('postPersonalInfo')
    cy.route('POST', "/applicant/smartmove/finret-generate-application").as('postSmartmove')
  
    cy.get('button[data-test="submit"]')
      .click()
    cy.wait(['@postPersonalInfo', '@postSmartmove'], {timeout: 30000})
  })
  
  it('Starts IDMA verification', () => {
    cy.get('button') //Start
      .click()
  
    cy.get('div label').first() //#1
      .click()
    cy.get('button') //next
      .click()
  
    cy.get('div label').first() //#2
      .click()
    cy.get('button') //next
      .click()
  
    cy.route('POST', "/applicant/smartmove/submit-idma-exam").as('submitIdma')
    cy.get('div label').first() //#3
    .click()
    cy.get('button') //submit
    .click()
    cy.wait('@submitIdma')
  
    cy.get('button') //continue
      .click()
    cy.wait(10000)
  })
})

describe('Payment', () => {
  it('Fills out Square Payment Form', function() {
    cy.get('input[placeholder="Name"]')
      .type(this.applicant.sqpaymentName)
    cy.pause()
    //cy.get('iframe[id="sq-card-number"]')
    //manual input required
  
    //resume here
    cy.route('POST', '/purchase/application').as('purchaseApplication')
    cy.get('button[data-test="purchase"]')
      .click()
    cy.wait('@purchaseApplication', {timeout: 10000})
  })
  /**
  it('Authorizes Financial Information Release', function() {
    cy.pause()
    //cy.get('button[data-test="authorize-financial-information-release"]')
    //manual input required
  
    /* cy.get('button[class="Button Button--is-plaid-color"]')
      .click()
  
    cy.get('li[data-institution="citi"]')
      .click()
  
    cy.get('input[placeholder="User ID"]')
      .type(this.newapplicant.bankUser)
  
    cy.get('input[placeholder="Password"]')
      .type(this.newapplicant.bankPassword)
  
    cy.route('POST', '/plaid/exchange-token').as('plaid')
    cy.get('button[type="submit"]') //submit
      .click()
    cy.get('button[data-test="continue"]') //continue
      .click()
    cy.wait('@plaid')
    cy.route('POST', '/applicant/application/share').as('shareApplication')
    cy.get('button[role="button"]')
      .click()
    cy.wait('@shareApplication')
  }) */
})
/**
describe('Finish Screen and GoToDashboard', () => {
  //Happy Test
  it('Loads Finish screen and can go to dashboard', function() {
    //resume here
    cy.get('button[data-test="review-application"]')
      .click()
  
    cy.location("pathname").should('eq', '/applicant/dashboard')
  })
})
 **/