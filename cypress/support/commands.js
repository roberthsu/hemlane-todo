Cypress.Commands.add("login", (email, password) => {
  let formData = new FormData()
  formData.append('_username', email)
  formData.append('_password', password)
  cy.request({
    method: 'POST',
    url: '/security/login',
    body: formData
  })
})

Cypress.Commands.add('fillPersonalInfoForm', (manager) => {
  cy.get('input[name=email]')
    .type(manager.goodEmail)

  cy.get('input[name=firstName]')
    .type(manager.goodFirstName)

  cy.get('input[name=lastName]')
    .type(manager.goodLastName)

  cy.get('input[name=password]')
    .type(manager.goodPassword)

  cy.get('input[name=confirmPassword]')
    .type(manager.goodPassword)

  cy.get('div[name=source]')
    .click()

  cy.get('div[role=option]')
    .first()
    .click({force:true})
})

Cypress.Commands.add('fillBusinessInfoForm', (manager) => {
  cy.get('input[name=businessName]')
    .type(manager.businessName)

  cy.get('div[class="react-tel-input"] input')
    .type(manager.goodPhoneNumber)

  cy.get('input[name=streetAddress]')
    .type(manager.goodStreetAddress)

  cy.get('input[name=aptNumber]')
    .type(manager.aptNumber)

  cy.get('input[name=city]')
    .type(manager.goodCity)

  cy.get('div[name=state] input')
    .type(manager.state)

  cy.get('input[name=zipCode]')
    .type(manager.zipCode)
})

Cypress.Commands.add('checkPersonalInfoForm', (manager) => {
  cy.get('h3')
    .should('contain.text', 'Add Your Personal Information')

  cy.get('input[name=email]')
    .should('have.value', manager.goodEmail)

  cy.get('input[name=firstName]')
    .should('have.value', manager.goodFirstName)

  cy.get('input[name=lastName]')
    .should('have.value', manager.goodLastName)

  cy.get('input[name=password]')
    .should('have.value', manager.goodPassword)

  cy.get('input[name=confirmPassword]')
    .should('have.value', manager.goodPassword)

    //TODO: deal with verifying dropdown selection
  // cy.get('div[name=source]')
})

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
