## Questions To Ask Yourself While Developing:

### Before Asking For Help:

##### Questions to ask yourself. Did you...

    Run an NPM install?

    Update your database?

    Make sure your imports work correctly?

    Make sure your app.bundle.js is a fresh build?

    Did you spend 15 minutes googling your question?

    Check dev tools for error logs?

    Check terminal for error messages?

    Ask your teammate? (collaboration is key!)

After trying to debug yourself and having spent 30 minutes on it (no more, no less), ask Allen or Robert for help.

### Before You Make A Pull-Request

#### Be sure to:

    git fetch upstream master

    git pull upstream master

    Resolve any merge conflicts

Pull-requests that haven't been synced up with the Master repository will immediately be rejected.

Ensure merge conflicts are double-checked and the proper commits are in the code.

### When You Make A Pull-Request

##### Questions to ask yourself. Did you...

    Make sure your code is DRY? (Don’t Repeat Yourself)

    Get rid of console.logs from error checking?

    Test your code (in desktop and mobile view)?

    Think of edge cases for issues that could be possible issues?

    Check the AJAX request using PostMan or Dev Network Tools?

    Check the data base behavior is expected?

    Check the file structure make sense?

    Check that the designs and your feature look the same?

    Demo to Allen or Robert, either in person or via screenshare?
