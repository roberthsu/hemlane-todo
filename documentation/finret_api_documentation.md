# Finret API Documentation

API base url: `https://api.finret.com/`

#### Best practice
It's recommended that you make server-side requests to keep your API key hidden. While the API key alone isn't enough to access your account, you may want to filter information before displaying them onto your site.

You'll need to save the applicationId in order to access to the application again in the future.
##### Example request

```
$ajax('https://api.finret.com/security/api/get-applications',
{
  Method: GET,
  Headers: {
    apiKey: '3gjhdf23rfsg2ffdjksfsgsh',
    Content-Type: application/json
  },
});
```

### Headers:
On top of white-listing your company's domain, insert this for headers for access to our API.

```
Headers: {
            apiKey: 'insertYourAPIKey',
            Content-Type: application/json,
         }
```

## Available Endpoints:
GET:`https://api.finret.com/security/api/get-fastlink-tokens/{applicationIdHash}`<br/>
GET:`https://api.finret.com/security/api/get-applications`<br/>
GET:`https://api.finret.com/security/api/get-application/{applicationIdHash}`<br/>
GET:`https://api.finret.com/security/api/get-report-details/{applicationIdHash}`<br/>
POST:`https://api.finret.com/security/api/create-application`<br/>
POST:`https://api.finret.com/security/api/delete-application/{applicationIdHash}`<br/>

## Endpoint Details


### Get Fastlink Token
endpoint:`https://api.finret.com/security/api/get-fastlink-tokens/{applicationIdHash}`<br/>
type: `GET`<br/>
input: `URL slug`<br/>
response:

```
{
  'userSessionToken' => (string),
  'userAccessTokens' => (string)
}
```

description: <br/>
*Fastlink tokens are used to allow authentication for `Yodlee`, our provider for banking information. Adding the tokens into the Yodlee ajax request will enable your applicant to securely log into their bank account. Once they've logged into their account, our system will receive the authorization and process the date.*

### Get Applications
endpoint:`https://api.finret.com/security/api/get-applications`<br/>
type:`GET`<br/>
input:`none`<br/>
response:`[array]`<br/>
description: <br/>
*This will return all applications that have been made by the account. This includes information on the applicants' identity, property they've applied to, and an `applicationIdHash`. Accounts are tied with a user's API key.*


### Get Single Application
endpoint:`https://api.finret.com/security/api/get-applications`<br/>
type:`GET`<br/>
input:`none`<br/>
response:`{obj}`<br/>
description: <br/>
*This will return a single application that have been made by the account. This includes information on the applicants' identity, property they've applied to, and an `applicationIdHash`. Accounts are tied with a user's API key.*


### Get Report Detail
endpoint:`https://api.finret.com/security/api/get-report-details/{applicationIdHash}`<br/>
type:`GET`<br/>
input:`URL slug`<br/>
response:`{obj}`<br/>
description: <br/>
*Each application that you pull from the API will have an included `applicationIdHash`. To view a detailed report covering 12 months of income/expenses, attach the `applicationIdHash` as a slug.*


### Create Application
endpoint:`https://api.finret.com/security/api/create-application`<br/>
type:`POST`<br/>
input:

```
body: {
        firstName: (required string),
        lastName: (required string),
        email: (required string),
        propertyAddress: (required string),
        rent: (required string),
      }
```
response: `string`<br/>
description:<br/>
*This allows you to add a new application to your account. The application works similarly to the required input in the dashboard. These details will be used to log the applicant as well as the property details.*

### Delete Application
endpoint:`https://api.finret.com/security/api/delete-application/{applicationIdHash}`<br/>
type: `POST`<br/>
input: `URL slug`<br/>
response: `string`<br/>
description:<br/>
*Similarly to how you view report details, you can delete applications by attaching the `applicationIdHash` as a slug.*
