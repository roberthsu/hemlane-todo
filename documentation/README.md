FinRET
==============

#Getting Started

##Check to make sure the following are installed
- Homebrew ($ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)")  
- PHP7 (should be native, check with `php -v`)  
- Be wary if MySQL version > 8.0 
- MySQL (brew install mysql@5.7)  
- Sequel Pro (http://www.sequelpro.com/)  

##Database set up
- start MySQL server `/usr/local/opt/mysql@5.7/bin/mysql.server start`  
- open Sequel Pro  
- log in with credentials  

```
	host: 127.0.0.1  
	username: root

```  
- hit connect  
- dropdown menu database -> add database... -> call it `finretv4`
- add a new tab for stage connection

```
  host: 206.189.79.250
  username: stage-app
  password: see params
```
- request to be added to stagedb's ip whitelist

## App specific setup
- install composer php package manager (brew install composer)  
- brew install node
- make sure you have Gulp installed  
  `npm install -g gulp`  
- fork finretOfficial/finretV4  
- clone your repository  

#### 2 ways of installing everything and getting the env set up:  
```
$ npm run initialize
```


or  

```
$ npm install   
$ composer install  
$ bin/console doc:sche:update --force
```


These commands will be necessary to get your app running for development. Do it in this order:

For local:
```
       $ npm run local
       $ npm run server
```
For prod:
```
       $ npm run build
```


```
Is this your first time setting up the project?
In the cobrandtoken table add in an entry that has:

token: null
creationDate: CURRENTTIMESTAMP
expirationDate: 2000-07-16 06:44:17
name: yodlee

```
Navigate to localhost:8000

## Deployment

Main server:

```
$ ssh [your_user_name]@206.189.169.204
$ password: [your_password]
```

Stage server:

```
$ ssh [your_user_name]@206.189.79.250
$ password: [your_password]
```
