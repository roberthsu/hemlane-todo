<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Action;
use AppBundle\Entity\Task;
use AppBundle\Entity\Ticket;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class TaskController extends Controller
{
    /**
     * @Route("/tasks/{ticketId}", name="tasksList")
     * @Method("GET")
     *
     * @param Request $request
     * @param $ticketId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tasksList(Request $request, $ticketId)
    {
        $em = $this->getDoctrine()->getManager();

        $taskRepository = $em->getRepository(Task::class);
        $tasks = $taskRepository->findBy(['ticket' => $ticketId]);

        $tasksData = [];

        /** @var Task $task */
        foreach($tasks as $task)
        {
            array_push($tasksData, [
                'id' => $task->getId(),
                'title' => $task->getTitle(),
                'description' => $task->getDescription(),
                'done' => $task->getDone(),
            ]);
        }

        return new JsonResponse($tasksData);
    }

    /**
     * @Route("/task/update", name="taskUpdate")
     * @Method("PATCH")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function taskUpdate(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $body = json_decode($request->getContent());

        $taskRepository = $em->getRepository(Task::class);
        /** @var Task $task */
        $task = $taskRepository->findOneBy(['id' => $body->id]);

        $task->setDone($body->done);

        $em->persist($task);
        $em->flush();

        return new Response('success', 200);
    }



}