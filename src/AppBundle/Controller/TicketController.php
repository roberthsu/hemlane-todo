<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Action;
use AppBundle\Entity\Task;
use AppBundle\Entity\Ticket;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class TicketController extends Controller
{
    /**
     * @Route("/ticket/list", name="ticketList")
     * @Method("GET")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function ticketList(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $ticketRepository = $em->getRepository(Ticket::class);
        $tickets = $ticketRepository->findAll();

        $data = [];
        /** @var Ticket $ticket */
        foreach($tickets as $ticket) {
            $data[] = [
                'id' => $ticket->getId(),
                'title' => $ticket->getTitle(),
                'description' => $ticket->getDescription(),
                'status' => $ticket->getStatus()
            ];
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/ticket/update", name="ticketUpdate")
     * @Method("PATCH")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ticketUpdate(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $body = json_decode($request->getContent());

        $ticketRepository = $em->getRepository(Ticket::class);
        /** @var Ticket $ticket */
        $ticket = $ticketRepository->findOneBy(['id' => $body->id]);

        $ticket->setStatus($body->status);

        $em->persist($ticket);
        $em->flush();

        return new Response('success', 200);
    }

    /**
     * @Route("/ticket/create", name="ticketCreate")
     * @Method("POST")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ticketCreate(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $body = json_decode($request->getContent());

        $ticket = new Ticket();
        $ticket->setTitle($body->title)
            ->setDescription($body->description)
            ->setDone(false)
            ->setStatus('troubleshooting');

        $em->persist($ticket);
        $em->flush();

        return new Response('success', 200);
    }
}