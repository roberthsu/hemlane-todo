<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Action;
use AppBundle\Entity\Task;
use AppBundle\Entity\Ticket;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class ActionController extends Controller
{
    /**
     * @Route("/actions/{taskId}", name="actionsList")
     * @Method("GET")
     *
     * @param Request $request
     * @param $ticketId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function actionsList(Request $request, $taskId)
    {
        $em = $this->getDoctrine()->getManager();

        $actionRepository = $em->getRepository(Action::class);
        $actions = $actionRepository->findBy(['task' => $taskId]);

        $actionData = [];

        /** @var Action $action */
        foreach($actions as $action)
        {
            array_push($actionData, [
                'id' => $action->getId(),
                'title' => $action->getTitle(),
                'description' => $action->getDescription(),
                'status' => $action->getStatus()
            ]);
        }

        return new JsonResponse($actionData);
    }

    /**
     * @Route("/action/update", name="actionUpdate")
     * @Method("PATCH")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function actionUpdate(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $body = json_decode($request->getContent());

        $actionRepository = $em->getRepository(Action::class);
        /** @var Ticket $ticket */
        $action = $actionRepository->findOneBy(['id' => $body->id]);

        $action->setStatus($body->status);

        $em->persist($action);
        $em->flush();

        return new Response('success', 200);
    }

}