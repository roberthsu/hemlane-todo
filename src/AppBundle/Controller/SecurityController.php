<?php
namespace AppBundle\Controller;

use Finret\DataRepositoryBundle\Entity\User\Applicant;
use Finret\DataRepositoryBundle\Entity\User\Manager;
use Finret\DataRepositoryBundle\Entity\PasswordResetToken;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use AppBundle\Service\Notification\SendgridService;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class SecurityController extends Controller
{
    /**
     * @Route("/security/login", name="login")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function loginAction(Request $request)
    {
        /** @var AuthenticationUtils $authenticationUtils */
        $authenticationUtils = $this->get('security.authentication_utils');
        $authenticationUtilsChecker = $this->get('security.authorization_checker');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        //if there's an error, send 401 status code, otherwise it's successful and causes redirect via front-end
        if ($error) {
            return new Response($error->getMessage(), 401);
        } else {
            return new Response(null, 200);
        }
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {}
}
