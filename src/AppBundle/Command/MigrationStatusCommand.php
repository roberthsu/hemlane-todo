<?php

namespace AppBundle\Command;


use AppBundle\Entity\Ticket;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrationStatusCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:migrate';

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getEntityManager();

        $ticketRepository = $em->getRepository(Ticket::class);
        $tickets = $ticketRepository->findAll();

        /** @var Ticket $ticket */
        foreach($tickets as $ticket) {
            $status = $ticket->getDone();
            if($status == 0)
            {
                $ticket->setStatus('troubleshooting');
            }
            else {
                $ticket->setStatus('done');
            }

            $em->persist($ticket);
            $em->flush();
        }
    }
}