<?php

namespace AppBundle\Entity;

trait DateTraits
{
  public function setTimestamps()
  {
    $ts = new \DateTime(date('Y-m-d H:i:s'));

    $this->setUpdateDate($ts);

    if ($this->getCreationDate() == null)
    {
      $this->setCreationDate($ts);
    }
  }
}