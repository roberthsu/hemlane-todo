import React, { Component } from "react";
import {
  Grid,
  Checkbox,
  Button
} from "semantic-ui-react";

import './style.css'
import Action from 'Components/Action'
import axios from "axios";

export default class Task extends Component {
  state = {
    actions: []
  }

  componentWillMount() {
    this.getActions()
  }

  getActions = () => {
    const { taskId } = this.props
    axios({
      method: 'GET',
      url: '/actions/' + taskId
    }).then((response) => {
      this.setState({actions: response.data})
    });
  }

  updateAction = (id, status, idx) => {
    axios({
      method: 'PATCH',
      url: '/action/update',
      data: {
        id: id,
        status: status
      }
    }).then((response) => {
      const actions = [...this.state.tasks]
      actions[idx] = {
        id: this.state.actions[idx].id,
        description: this.state.actions[idx].description,
        title: this.state.actions[idx].title,
        status: status
      }
      this.setState({actions: actions})
    });
  }

  render() {
    const { taskId, title, description, done, taskIdx, updateTask } = this.props
    const { actions } = this.state

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column width={3}/>
          <Grid.Column width={7}>
            <Grid.Row>
              <h3>{ title }</h3>
            </Grid.Row>
            <Grid.Row>
              <p>{ description }</p>
            </Grid.Row>
          </Grid.Column>
          <Grid.Column width={6}>
            <Checkbox
              defaultChecked={done}
              onClick={() => { updateTask(taskId, !done, taskIdx)}}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          {
            actions.map((action, idx) => {
              return <Action
                key={action.id}
                id={action.id}
                title={action.title}
                description={action.description}
                status={action.status}
                idx={idx}
                updateAction={this.updateAction}
              />
            })
          }
        </Grid.Row>

      </Grid>

    );
  }
}
