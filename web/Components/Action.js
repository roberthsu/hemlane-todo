import React, { Component } from "react";
import {
  Grid,
  Dropdown
} from "semantic-ui-react";

import './style.css'

export default class Action extends Component {
  render() {
    const { id, title, description, status, idx, updateAction } = this.props

    const options = [
      {
        key: 'troubleshooting',
        text: 'Trouble Shooting',
        value: 'troubleshooting',
      },
      {
        key: 'inprogress',
        text: 'In Progress',
        value: 'inprogress',
      },
      {
        key: 'done',
        text: 'Done',
        value: 'done',
      }
    ]

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column width={6}/>
          <Grid.Column width={5}>
            <Grid.Row>
              <h3>{ title }</h3>
            </Grid.Row>
            <Grid.Row>
              <p>{ description }</p>
            </Grid.Row>
          </Grid.Column>
          <Grid.Column width={5}>
            <Dropdown
              placeholder='Status'
              selection
              defaultValue={status}
              options={options}
              onChange={(e, {key, value}) => {updateAction(id, value, idx)}}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>

    );
  }
}
