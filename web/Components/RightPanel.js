import React, { Component } from "react";
import {
  Grid
} from "semantic-ui-react";

import './style.css'

export default class RightPanel extends Component {

  updateTicket = (done, id) => {

  }

  render() {
    const { activeIdx, title, description } = this.props

    return (

      <Grid>
        {
          activeIdx >= 0 ?
            <Grid.Row>
                <Grid.Row>
                  <h3>{ title }</h3>
                </Grid.Row>
                <Grid.Row>
                  <p>{ description }</p>
                </Grid.Row>
            </Grid.Row>
            :
            <p>nothing is selected</p>

        }

      </Grid>

    );
  }
}
