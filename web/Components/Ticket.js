import React, { Component } from "react";
import {
  Grid,
  Dropdown
} from "semantic-ui-react";

import Task from 'Components/Task.js'

import './style.css'
import axios from "axios";

export default class Ticket extends Component {
  state = {
    tasks: []
  }

  componentWillMount() {
    this.getTasks()
  }

  getTasks = () => {
    const { id } = this.props
    axios({
      method: 'GET',
      url: '/tasks/' + id
    }).then((response) => {
      this.setState({tasks: response.data})
    });
  }

  updateTask = (id, done, idx) => {
    axios({
      method: 'PATCH',
      url: '/task/update',
      data: {
        id: id,
        done: done
      }
    }).then((response) => {
      const tasks = [...this.state.tasks]
      tasks[idx] = {
        id: this.state.tasks[idx].id,
        description: this.state.tasks[idx].description,
        title: this.state.tasks[idx].title,
        done: done
      }
      this.setState({tasks: tasks})
    });
  }

  render() {
    const { id, title, description, status, idx, updateTicket, isActive, toggleActive } = this.props
    const { tasks } = this.state

    const options = [
      {
        key: 'troubleshooting',
        text: 'Trouble Shooting',
        value: 'troubleshooting',
      },
      {
        key: 'inprogress',
        text: 'In Progress',
        value: 'inprogress',
      },
      {
        key: 'done',
        text: 'Done',
        value: 'done',
      }
    ]

    return (
      <Grid className={`${ isActive ? 'gray-background' : null}`}>
        <Grid.Row onClick={() => toggleActive(idx, title, description)}>
          <Grid.Column width={6}>
            <Grid.Row>
              <h3>{ title }</h3>
            </Grid.Row>
            <Grid.Row>
              <p>{ description }</p>
            </Grid.Row>
          </Grid.Column>
          <Grid.Column width={6}>
            <Dropdown
              placeholder='Status'
              selection
              defaultValue={status}
              options={options}
              onChange={(e, {key, value}) => {updateTicket(id, value, idx)}}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          {
            tasks.map((task, taskIdx) => {
              return <Task
                key={task.id}
                taskId={task.id}
                title={task.title}
                description={task.description}
                done={task.done}
                taskIdx={taskIdx}
                actionIds={task.actionIds}
                updateTask={this.updateTask}
              />
            })
          }
        </Grid.Row>

      </Grid>

    );
  }
}
