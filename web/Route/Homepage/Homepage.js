import React, { Component } from 'react';
import {
  Grid,
  Button,
  Form,
  Dropdown
} from 'semantic-ui-react';

import axios from 'axios'

import Ticket from 'Components/Ticket.js'
import RightPanel from 'Components/RightPanel.js'

export default class Homepage extends Component {
  state = {
    filter: 'all',
    title: '',
    description: '',
    data: [],
    activeIdx: -1,
    panelTitle: '',
    panelDescription: ''
  };

  componentWillMount() {
    this.getTickets()
    this.parseQueryString()
  }

  parseQueryString() {
    if (window.location.search.includes('filter')) {
      const status = window.location.search.split('filter=')[1].split('&')[0]
      this.setState({filter: status})
    }

    if (window.location.search.includes('activeIdx')) {
      const activeIdx = window.location.search.split('activeIdx=')[1].split('&')[0]
      this.setState({activeIdx: parseInt(activeIdx)})
    }
  }

  updateQueryString = (filter, activeIdx) => {
    window.location.replace(window.location.origin + '?filter=' + filter + '&activeIdx=' + activeIdx)
  }

  toggleActive = (ticketIdx, title, description) => {
    this.state.activeIdx === ticketIdx ?
      this.setState({activeIdx: -1})
      :
      this.setState({
        activeIdx: ticketIdx,
        panelTitle: title,
        panelDescription: description
      })

    this.updateQueryString(this.state.filter, ticketIdx)
  }

  getTickets = () => {
    axios({
      method: 'GET',
      url: '/ticket/list'
    }).then((response) => {
      this.setState({data: response.data})
    });
  }

  updateTicket = (id, status, idx) => {
    axios({
      method: 'PATCH',
      url: '/ticket/update',
      data: {
        id: id,
        status: status
      }
    }).then((response) => {
      const data = [...this.state.data]
      data[idx] = {
        id: this.state.data[idx].id,
        description: this.state.data[idx].description,
        title: this.state.data[idx].title,
        status: !this.state.data[idx].status,
        tasks: this.state.data[idx].tasks
      }
      this.setState({data: data})
    });
  }

  createTicket = () => {
    axios({
      method: 'POST',
      url: '/ticket/create',
      data: {
        title: this.state.title,
        description: this.state.description,
      }
    }).then((response) => {
      this.setState({
        title: '',
        description: ''
      })
      this.getTickets()
    });
  }

  handleChange = (e, {name, value}) => {
    this.setState({
      [name]: value
    })
  }

  handleFilterChange = (e, { value }) => {
    this.setState({filter: value})
    this.updateQueryString(value, this.state.activeIdx)

  }

  render() {
    const options = [
      {
        key: 'all',
        text: 'All',
        value: 'all',
      },
      {
        key: 'troubleshooting',
        text: 'Trouble Shooting',
        value: 'troubleshooting',
      },
      {
        key: 'inprogress',
        text: 'In Progress',
        value: 'inprogress',
      },
      {
        key: 'done',
        text: 'Done',
        value: 'done',
      }
    ]

    const { filter, activeIdx } = this.state

    return (
      <Grid>
        <Grid.Column width={2}/>
        <Grid.Column width={14}>
          <Grid.Row>
            <h3>To Do App</h3>
          </Grid.Row>
          <Grid.Row>
            <Form>
              <Form.Input label='title'
                          value={this.state.title}
                          name='title'
                          onChange={this.handleChange}
              />
              <Form.Input label='description'
                          value={this.state.description}
                          name='description'
                          onChange={this.handleChange}
              />
            </Form>
            <Button onClick={this.createTicket}>Create Ticket</Button>
          </Grid.Row>
          <Grid.Row>
            <Dropdown
              placeholder='Status'
              selection
              defaultValue={filter}
              options={options}
              onChange={this.handleFilterChange}
            />
          </Grid.Row>
        </Grid.Column>

        <Grid.Row>
          <Grid.Column width={2}/>
          <Grid.Column width={6}>
          {
            this.state.data.filter((ticket) => {
              if(this.state.filter === 'all') {
                return true
              }
              return this.state.filter === ticket.status
            }).map((ticket, idx) => {
              return <Ticket
                key={ticket.id}
                id={ticket.id}
                idx={idx}
                title={ticket.title}
                description={ticket.description}
                status={ticket.status}
                isActive={activeIdx === idx}
                updateTicket={this.updateTicket}
                toggleActive={this.toggleActive}
              />
            })
          }
          </Grid.Column>
          <Grid.Column width={6}>
                <RightPanel
                  activeIdx={activeIdx}
                  title={this.state.panelTitle}
                  description={this.state.panelDescription}
                />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}
