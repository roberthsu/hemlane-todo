const routes = {
  baseUrl: window.location.origin,

  //security URLs
  loginUrl: '/security/login',
  logoutUrl: '/logout',
};

export default routes;
